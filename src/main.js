// =========================================================
// * Vuetify Material Dashboard - v2.1.0
// =========================================================
//
// * Product Page: https://www.creative-tim.com/product/vuetify-material-dashboard
// * Copyright 2019 Creative Tim (https://www.creative-tim.com)
//
// * Coded by Creative Tim
//
// =========================================================
//
// * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './plugins/base'
import './plugins/chartist'
import './plugins/vee-validate'
import vuetify from './plugins/vuetify'
import i18n from './i18n'
import VueQrcode from '@chenfengyuan/vue-qrcode';
import VueScheduler from 'v-calendar-scheduler';
import 'v-calendar-scheduler/lib/main.css';
import { SchedulePlugin } from '@syncfusion/ej2-vue-schedule';

Vue.use(SchedulePlugin);
Vue.component(VueQrcode.name, VueQrcode);
Vue.config.productionTip = false
Vue.use(VueScheduler);
new Vue({
  router,
  store,
  vuetify,
  i18n,
  render: h => h(App),
}).$mount('#app')
