import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    barColor: 'rgba(0, 0, 0, .8), rgba(0, 0, 0, .8)',
    // barImage: 'https://demos.creative-tim.com/material-dashboard/assets/img/sidebar-1.jpg',
    barImage: 'https://lh3.googleusercontent.com/proxy/MrRGcNGEHcVzD4HjevvRWxBosHx1avsGgICOSzhVQGjUZozEVY9Ihy1jp-e8M1lhgP7U0OVz25AE9DQcW5fhUhBfhWadVnYn0AwdWQsywgg4rQ',
    userType: 'Admin',
    drawer: null,
    asset: null,
  },
  mutations: {
    SET_BAR_IMAGE (state, payload) {
      state.barImage = payload
    },
    SET_DRAWER (state, payload) {
      state.drawer = payload
    },
    SET_USER_TYPE (state, payload) {
      state.userType = payload
    },
    SET_ASSET (state, payload) {
      state.asset = payload
    },
  },
  actions: {

  },
})
