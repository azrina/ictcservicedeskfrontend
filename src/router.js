import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: "history",
  routes: [
   
    {
      name: 'login',
      path: '/login',
      component: () => import('@/views/Login'),
    },
     {
      path: '/',
      component: () => import('@/views/dashboard/Index'),
      children: [
        {
          name: 'Admin Dashboard',
          path: 'admin',
          component: () => import('@/views/Admin/Dashboard'),
        },
        // Dashboard
        {
          name: 'Dashboard',
          path: '',
          component: () => import('@/views/dashboard/Dashboard'),
        },
        // Asset Dashboard
        {
          name: 'Asset Dashboard',
          path: 'asset',
          component: () => import('@/views/Admin/Asset/Dashboard'),
        },
         // Asset Details
         {
          name: 'Asset Details',
          path: 'asset/details',
          component: () => import('@/views/Admin/Asset/AssetDetails'),
        },
        {
          name: 'Asset Edit',
          path: 'asset/edit/:asset_id',
          component: () => import('@/views/Admin/Asset/AssetDetailEdit'),
        },
        {
          name: 'Asset Add',
          path: 'asset/add',
          component: () => import('@/views/Admin/Asset/AssetAdd'),
        },
        {
          name: 'License',
          path: 'license',
          component: () => import('@/views/Admin/License/Dashboard'),
        },
        {
          name: 'Ticket',
          path: 'ticket',
          component: () => import('@/views/Admin/Ticket/Dashboard'),
        },
        {
          name: 'Issue Ticket Edit',
          path: 'ticket/issue/edit/:ticket_id',
          component: () => import('@/views/Admin/Ticket/IssueTicketEdit'),
        },
        {
          name: 'Request Ticket Edit',
          path: 'ticket/request/edit/:ticket_id',
          component: () => import('@/views/Admin/Ticket/RequestTicketEdit'),
        },
        {
          name: 'Technician Leave Schedule',
          path: 'schedule',
          component: () => import('@/views/Admin/Schedule/LeaveSchedule'),
        },
        {
          name: 'Staff Dashboard',
          path: 'staff',
          component: () => import('@/views/Admin/Staff/Dashboard'),
        },
        {
          name: 'Staff Details',
          path: 'staff/details/:staff_id',
          component: () => import('@/views/Admin/Staff/StaffDetails'),
        },
        {
          name: 'Staff Details Edit',
          path: 'staff/edit/:asset_id',
          component: () => import('@/views/Admin/Staff/StaffDetailEdit'),
        },
        {
          name: 'Reports',
          path: 'reports',
          component: () => import('@/views/Admin/Reports/Dashboard'),
        },
        {
          name: 'Settings',
          path: 'settings',
          component: () => import('@/views/Admin/Settings/Dashboard'),
        },
        // Pages
        {
          name: 'User Profile',
          path: 'pages/user',
          component: () => import('@/views/dashboard/pages/UserProfile'),
        },
        {
          name: 'Notifications',
          path: 'components/notifications',
          component: () => import('@/views/dashboard/component/Notifications'),
        },
        {
          name: 'Icons',
          path: 'components/icons',
          component: () => import('@/views/dashboard/component/Icons'),
        },
        {
          name: 'Typography',
          path: 'components/typography',
          component: () => import('@/views/dashboard/component/Typography'),
        },
        // Tables
        {
          name: 'Regular Tables',
          path: 'tables/regular-tables',
          component: () => import('@/views/dashboard/tables/RegularTables'),
        },
        // Maps
        {
          name: 'Google Maps',
          path: 'maps/google-maps',
          component: () => import('@/views/dashboard/maps/GoogleMaps'),
        },
        // Upgrade
        {
          name: 'Upgrade',
          path: 'upgrade',
          component: () => import('@/views/dashboard/Upgrade'),
        },
      
      ],
    },
  ],
})
