"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var lodash_1 = __importDefault(require("lodash"));
var debug_1 = __importDefault(require("debug"));
var debug = debug_1.default('cypress:proxy:http:error-middleware');
var LogError = function () {
    debug('error proxying request %o', lodash_1.default.pick(this, 'error', 'req', 'res', 'incomingRes', 'outgoingReq', 'incomingResStream'));
    this.next();
};
exports.AbortRequest = function () {
    if (this.outgoingReq) {
        debug('aborting outgoingReq');
        this.outgoingReq.abort();
    }
    this.next();
};
exports.UnpipeResponse = function () {
    if (this.incomingResStream) {
        debug('unpiping resStream from response');
        this.incomingResStream.unpipe();
    }
    this.next();
};
exports.DestroyResponse = function () {
    this.res.destroy();
    this.end();
};
exports.default = {
    LogError: LogError,
    AbortRequest: exports.AbortRequest,
    UnpipeResponse: exports.UnpipeResponse,
    DestroyResponse: exports.DestroyResponse,
};
